# Stuff and Storage

## Variables
Programs need information to work with and we store that information in **variable**.

A **variable** is simply a location or box where information can be stored.  

The **variable** will have a name so we can get that information later.

## Data Type
What type of stuff can we put in that box?

### Numbers
- Whole Numbers: integer or int
- Fractional Numbers: floating point or float

### Sequences
- Text: string or string
     - Sequence of characters
- Updatable Collection: list
     - List of changable stuff
- Non-Updatable Collection: tuple
    - List of stuff that cannot change