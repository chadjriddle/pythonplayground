## Basic Programming Concepts

- Tell Why (Comments)
- Stuff and Storage (Variables)
- Follow the Leader (Control Flow)
- Don't Repeat Yourself (Functions)
- Don't Re-Invent the Wheel (Modules)
- Smarter Stogage (Data Structures)
- Keep You Hands to Yourself (Classes)

## Basic Development Concepts
- Program Editor
- Program Debugger
- Source Control


## Basic Game Dev Concepts
- Game Setup
- Display Setup
- Time and Framerate
- Game Loop
- User Input Controls
- Colors
- Coordinate Systems
- Accessing Files
- Drawing Images
- Blitting 
- Drawing Text

## Asset Creation
- Images
- Sounds


## General Concepts

- 80/20 Rule
- If it Doesn't Add Something, It's Taking Away
