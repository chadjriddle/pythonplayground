import pygame
import os
import time

# Setup Constants
display_width = 800
display_height = 400

# Define Directories
base_directory = os.path.dirname(os.path.realpath(__file__))
image_directory = os.path.join(base_directory, "Images")
font_directory = os.path.join(base_directory, "Fonts")

# Colors are defined with Tuples - A non-changable list of stuff
# that define the Red, Green and Blue values of the color
black = (0,0,0)  
white = (255,255,255)

# Setup PyGame and Display
pygame.init()
gameDisplay = pygame.display.set_mode((display_width, display_height))
pygame.display.set_caption('My Racing Game')

#Setup Clock
clock = pygame.time.Clock()

# Setup Game Variables
exitGame = False

# Define Methods
def loadImage(filename):
    """ Load an image from the Image Directory and return the Surface """
    return pygame.image.load(os.path.join(image_directory, filename))

def drawImage(image, x, y):
    """ Draw an image at the specified location on the gameDisplay """
    gameDisplay.blit(image, (x, y))

def rotateCenter(image, angle):
    """ Rotate an image around it's center by the given angle and return a new Surface """
    loc = image.get_rect().center  #rot_image is not defined 
    rot_sprite = pygame.transform.rotate(image, angle)
    rot_sprite.get_rect().center = loc
    return rot_sprite

def loadFont(filename, size):
    return pygame.font.Font(os.path.join(font_directory, filename), size)

def createTextObjects(text, font):
    textSurface = font.render(text, True, black)
    return textSurface, textSurface.get_rect()

def displayMessage(text, size, delay):
    font = loadFont("Roboto-Bold.ttf", size)
    textSurf, textRect = createTextObjects(text, font)
    textRect.center = ((display_width/2),(display_height/2))
    gameDisplay.blit(textSurf, textRect)
    pygame.display.update()
    time.sleep(delay)
    

# Setup images
blockImage = loadImage("purple_square.png")
carImage = rotateCenter(loadImage("car2.png"), 90)

# Run Game Loop
while not exitGame:
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            exitGame = True
        if event.type == pygame.KEYDOWN:
            if event.key == pygame.K_m:
                displayMessage("You Pressed M", 100, 2)
    
    # Clear the Display
    gameDisplay.fill(white)

    # Draw Images
    drawImage(blockImage, 100, 100)
    drawImage(carImage, 300, 200)

    # Update the Display
    pygame.display.update()

    # Update the Clock
    clock.tick(60)


# Exit the Game
pygame.quit() # exit pygame
quit() # exit the app



