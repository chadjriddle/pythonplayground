import pygame

# Setup Constants
display_width = 800
display_height = 400

# Colors are defined with Tuples - A non-changable list of stuff
# that define the Red, Green and Blue values of the color
black = (0,0,0)  
white = (255,255,255)

# Setup PyGame and Display
pygame.init()
gameDisplay = pygame.display.set_mode((display_width, display_height))
pygame.display.set_caption('My Racing Game')

#Setup Clock
clock = pygame.time.Clock()

# Setup Game Variables
exitGame = False

while not exitGame:
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            exitGame = True
    
    # Clear the Display
    gameDisplay.fill(white)

    # Update the Display
    pygame.display.update()

    # Update the Clock
    clock.tick(60)


# Exit the Game
pygame.quit() # exit pygame
quit() # exit the app



