import pygame
import os

# Setup Constants
display_width = 800
display_height = 400

# Define Directories
base_directory = os.path.dirname(os.path.realpath(__file__))
image_directory = os.path.join(base_directory, "Images")

# Colors are defined with Tuples - A non-changable list of stuff
# that define the Red, Green and Blue values of the color
black = (0,0,0)  
white = (255,255,255)

# Setup PyGame and Display
pygame.init()
gameDisplay = pygame.display.set_mode((display_width, display_height))
pygame.display.set_caption('My Racing Game')

#Setup Clock
clock = pygame.time.Clock()

# Setup Game Variables
exitGame = False

# Define Methods

def loadImage(filename):
    return pygame.image.load(os.path.join(image_directory, filename))

def drawImage(image, x, y):
    gameDisplay.blit(image, (x, y))

def rot_center(image, angle):
    """rotate a Surface, maintaining position."""

    loc = image.get_rect().center  #rot_image is not defined 
    rot_sprite = pygame.transform.rotate(image, angle)
    rot_sprite.get_rect().center = loc
    return rot_sprite


# Setup images
blockImage = loadImage("purple_square.png")
carImage = rot_center(loadImage("car2.png"), 90)


# Run Game Loop
while not exitGame:
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            exitGame = True
    
    # Clear the Display
    gameDisplay.fill(white)

    # Draw Images
    drawImage(blockImage, 100, 100)
    drawImage(carImage, 300, 200)

    # Update the Display
    pygame.display.update()

    # Update the Clock
    clock.tick(60)


# Exit the Game
pygame.quit() # exit pygame
quit() # exit the app



